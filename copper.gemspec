# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'copper/version'

Gem::Specification.new do |spec|
  spec.name          = "copper"
  spec.version       = Copper::VERSION
  spec.authors       = ["Tomoya Chiba"]
  spec.email         = ["tomo.asleep@gmail.com"]
  spec.description   = %q{VHDL test tool}
  spec.summary       = %q{VHDL test tool}
  spec.homepage      = ""
  spec.license       = "MIT"

  spec.files         = `git ls-files`.split($/)
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = ["lib"]

  spec.bindir = 'bin'
  spec.executables = ['copper']

  spec.add_development_dependency "bundler", "~> 1.3"
  spec.add_development_dependency "rake"
  spec.add_development_dependency "rspec"
  spec.add_development_dependency "pry"
end
