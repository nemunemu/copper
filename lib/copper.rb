require 'forwardable'
require 'ostruct'
require 'tmpdir'

require "copper/version"
require 'copper/extensions'
require 'copper/helper'

require 'copper/circuit'
require 'copper/vhdl_parser'
require 'copper/scenario'
require 'copper/tester'

require 'copper/view'
require 'copper/runner'
require 'copper/vhdl_sources'
require 'copper/vhdl_formatter'

require 'copper/command_line'
require 'copper/options'

module Copper
  class << self
    include Copper
  end

  attr_accessor :debug_mode
  def load_vhdl(path)
    VhdlSources.sources.add(path)
  end

  def debug_mode?
    @debug_mode
  end
end
