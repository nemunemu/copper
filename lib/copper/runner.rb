module Copper
  class Runner
    class << self
      def runner
        @runner ||= new
      end

      def add(description)
        runner.descriptions << description
      end
    end

    attr_accessor :descriptions
    def initialize
      @descriptions = []
    end

    def run(script_pathes, options)
      Scenario.read(script_pathes)
      scenarios = descriptions.map { |desc| Scenario.generate(desc) }
      results = Tester.run(scenarios, options)
      results.each { |result| run_fail(result.description) if result.failed? }
    end

    def run_fail(description)
      if Copper.debug_mode
        raise result.description
      else
      end
    end
  end
end
