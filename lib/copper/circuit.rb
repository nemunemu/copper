module Copper
  module Circuit
    require 'copper/circuit/port_factory'

    require 'copper/circuit/constant'
    require 'copper/circuit/entity'
    require 'copper/circuit/entity_wrapper'
    require 'copper/circuit/port'
    require 'copper/circuit/empty_port'
    require 'copper/circuit/record_port'
    require 'copper/circuit/record_element_port'
    require 'copper/circuit/generic'
    require 'copper/circuit/package'

    class << self
      include Circuit
    end

    def read(*pathes)
      pathes.map do |path|
        Circuit.from_parser(VhdlParser.read(path))
      end
    end

    def from_parser(env, parser)
      Entity.from_entity_parser(env, parser)
    end
  end
end
