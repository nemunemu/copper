module Copper::Scenario
  class Scenario
    attr_reader :description, :presenters
    def initialize(description, presenters)
      @description = description
      @presenters = presenters
    end

    def test_bench_name
      circuit.test_bench_name
    end

    def dut_name
      circuit.name
    end

    def dependencies
      [circuit.package_pathes, circuit.component_pathes].uniq.compact + [File.expand_path('copper_utils.vhd', __dir__), circuit.dut_path]
      # description.circuit.dependencies
    end

    def dir
      @dir ||= Dir.mktmpdir
    end

    def runner_script
      @pathes ||= presenters.map do |pre|
        path = File.expand_path("#{pre.circuit.test_bench_name}.vhd", dir)
        pre.view.save(path).path
      end
      RunnerScript.new(circuit.test_bench_name, dir, @pathes + dependencies)
    end

    def circuit
      description.circuit
    end
    
    def do_before
      description.do_before
    end
  end
end
