module Copper::Scenario
  class Assert
    attr_reader :assert
    def initialize(assert = {})
      @assert = assert
    end

    def add(port_values)
      @assert = (@assert || {}).merge(port_values)
    end

    def assertion(context, assign)
      return '' if @assert.length == 0
      %Q{assert #{condtions.join(' and ')} report "#{ "In context #{context}, " if context }FAILED: #{escape(assign.format)} expected to #{escape(condtions.join(', '))}, but #{actual}" severity warning;\n}
    end

    def in(entity)
      Assert.new(@assert.select { |k, v| entity.ports.include?(k) })
    end

    private
    def escape(str)
      str.gsub('"', '')
    end

    def condtions
      @assert.map { |port, value| port.equation(value) }
    end

    def actual
      @assert.map { |port, value| port.actual(value) }.join(", ")
    end
  end
end
