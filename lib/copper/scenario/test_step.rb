module Copper::Scenario
  class TestStep
    WAIT = 0.5
    def initialize(&block)
      @assign = Assign.new
      @assert = Assert.new
      instance_exec(self, &block)
    end

    def assign(port_values)
      @assign.add(port_values)
    end

    def assert(port_values)
      @assert.add(port_values)
    end

    def format(entity, context_word)
      assign = @assign.in(entity)
      assert = @assert.in(entity)

      ['',
       assign.format("\n"),
       '',
       assert.assertion(context_word, @assign),
       ''].join("\nwait for #{WAIT} ns;\n")
    end
  end
end
