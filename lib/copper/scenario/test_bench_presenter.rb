module Copper::Scenario
  class TestBenchPresenter
    def self.from_description(description)
      [description.circuit].map { |circuit| new(description, circuit) }
    end

    attr_reader :description, :circuit
    def initialize(description, circuit)
      @description = description
      @circuit = circuit
    end

    def format
      view.run
    end

    def view
      @view ||= Copper::View.new('test_bench.vhd.erb', self)
    end

    def scenario
      description.format(circuit)
    end

    def component_definition
      Copper::VhdlFormatter.component_definition(circuit)
    end

    def signal_definitions
      Copper::VhdlFormatter.signal_definitions(circuit)
    end

    def mappings
      Copper::VhdlFormatter.mappings(circuit)
    end

    def dependency_libraries
      circuit.packages
    end
    
    def clock_process
      clock = circuit.clock
      Copper::View.new(
        'clock.vhd.erb', ClockPresenter.new(clock.name)
      ).run unless clock.is_a? Copper::Circuit::EmptyPort
    end
  end

  class ClockPresenter
    attr_reader :clock_name
    def initialize(clock_name)
      @clock_name = clock_name
    end
  end
end
