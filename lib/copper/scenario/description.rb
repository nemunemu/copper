module Copper::Scenario
  class Description
    attr_reader :steps, :circuit, :dut
    protected :steps
    def initialize(circuit, options = {}, context = nil, parent = nil, &block)
      @circuit = circuit
      @parent = parent
      @context = context
      @options = options

      @events = []
      @dut = Copper::Circuit::EntityWrapper.new(circuit)
      instance_exec(@dut, &block) if block
    end
    public

    def step(*port_values, &proc)
      @events << TestStep.new(&proc)
      self
    end

    def wait_for(time)
      @events << Wait.new(time)
    end

    def context(name = nil, &block)
      @events << self.class.new(@circuit, {}, name, self, &block)
      self
    end

    def context_word
      (@parent && @parent.context_word) ? (@parent.context_word + ' ' + @context.to_s) : @context
    end

    def format(entity)
      @events.map do |ev|
        case ev
        when Description
          ev.format(entity)
        else
          ev.format(entity, context_word)
        end
      end.flatten.join("\n")
    end

    def do_before
      @options[:before].call(self) if @options[:before]
    end

    protected
    def root
      @root ||= (@parent ? @parent.root : self)
    end
  end
end
