module Copper::Scenario
  class Assign
    attr_reader :assigns
    def initialize(assigns = {})
      @assigns = assigns
    end

    def add(port_values)
      @assigns = (@assigns || {}).merge(port_values)
    end

    def format(div = ', ')
      @assigns.map { |port, value| port.assign(value) }.join(div)
    end

    def in(entity)
      Assign.new(@assigns.select { |k, v| entity.ports.include?(k) })
    end
  end
end
