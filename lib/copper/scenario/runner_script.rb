module Copper::Scenario
  class RunnerScript
    attr_reader :unit_name, :work_dir, :pathes
    def initialize(unit_name, work_dir, pathes)
      @unit_name = unit_name
      @pathes = pathes
      @work_dir = work_dir
    end

    def view
      @view ||= Copper::View.new('runner_script.sh.erb', self)
    end

    def save(path)
      view.save(path)
    end
  end
end
