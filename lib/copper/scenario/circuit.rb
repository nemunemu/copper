module Copper::Scenario
  class Circuit
    extend Forwardable

    def self.configure(name, options = {}, &block)
      env = Copper::VhdlSources.sources
      dut = env.find_entity(name)
      new(dut, options, env, &block)
    end

    def_delegators :@dut, :name, :ports, :generics
    def initialize(dut, options = {}, env = Copper::VhdlSources.sources, &block)
      @dut = dut
      @env = env
      @options = options
      instance_exec(Copper::Circuit::EntityWrapper.new(@dut), &block) if block
    end
    
    def test_bench_name
      "test_#{@dut.name}"
    end

    def clock(port = nil)
      port ? @clock = port : (@clock ||= Copper::Circuit::EmptyPort.new)
    end

    def dut_path
      @env.path_of_entity(name)
    end

    def scenario(options = {}, &proc)
      desc = Description.new(self, @options.merge(options), &proc)
      Copper::Runner.add(desc)
    end

    def components
      @dut.components(@env)
    end

    def packages
      @dut.packages(@env)
    end

    def component_pathes
      @dut.component_pathes(@env)
    end

    def package_pathes
      @dut.package_pathes(@env)
    end
  end
end
