module Copper::Scenario
  class Wait
    CLOCK_LENGTH = 2

    def initialize(length)
      @length = length
    end

    def format(*args)
      "wait for #{@length * CLOCK_LENGTH} ns;"
    end
  end
end
