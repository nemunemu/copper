module Copper::Circuit
  module PortFactory
    def from_element(parent, element, direction = nil)
      direction ||= element.direction
      type = element.type.origin(parent.root)
      factory(type).new(parent, element.name, direction, type)
    end

    def generator(name, direction, type)
      lambda { |parent| factory(type).new(parent, name, direction, type) }
    end

    def factory(type)
      case type
      when Copper::VhdlParser::Type::Record, Copper::VhdlParser::Type::Array
        RecordPort
      else
        Port
      end
    end
  end
end
