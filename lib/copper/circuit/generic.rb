module Copper::Circuit
  class Generic
    class << self
      def from_element(entity, element)
        type = element.type.origin(entity.root)
        new(entity, element.name, type, element.init_value)
      end
    end

    attr_reader :name, :type, :init_value
    def initialize(entity, name, type, init_value)
      @entity = entity
      @name = name
      @type = type
      @init_value = init_value
    end
  end
end
