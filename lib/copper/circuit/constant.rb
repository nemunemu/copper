module Copper::Circuit
  class Constant
    class << self
      def from_element(root, element)
        type = element.type.origin(root)
        new(entity, element.name, type, element.init_value)
      end
    end

    attr_reader :name, :type, :init_value
    def initialize(name, type, init_value = nil)
      @name = name
      @type = type
      @init_value = init_value
    end
  end
end
