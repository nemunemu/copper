module Copper::Circuit
  class EntityWrapper
    def initialize(entity)
      @entity = entity
    end

    def method_missing(action, *args)
      action_name = action.to_s
      port = @entity.ports.find {|p| p.name == action_name }
      if port
        port
      else
        raise "undefined port '#{action_name}'"
      end
    end
  end
end
