module Copper::Circuit
  class RecordPort < Port
    class << self
      include PortFactory

      def factory(type)
        case type
        when Copper::VhdlParser::Type::Record, Copper::VhdlParser::Type::Array
          RecordPort
        else
          RecordElementPort
        end
      end
    end
    def initialize(root, name, direction, type)
      super
      @elements = type.elements.map do |el|
        [el.name, RecordPort.from_element(self, el, direction)]
      end.to_h
    end
    
    def name
      case @name
      when String, Symbol
        @root.is_a?(Port) ? (@root.name + '.' + @name) : @name
      else
        @root.is_a?(Port) ? (@root.name + "(#@name)") : "(#@name)"
      end
    end

    def format(formatter, value)
      case value
      when Hash
        @elements.map do |el_name, el|
          formatter.call(el.name, el.type.format(value[el_name])) if value.member?(el_name)
        end.compact
      when Array
        @elements.zip(value).map do |(_name, el), val|
          el.format(formatter, val)
        end
      end
    end

    def length
      @elements.length
    end

    protected
    def entity
      @root.entity
    end
  end
end
