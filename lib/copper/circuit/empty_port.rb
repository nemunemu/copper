module Copper::Circuit
  class EmptyPort < Port
    def initialize
    end

    def root
      nil
    end

    def format(formatter, value)
      ''
    end

    def type
      Copper::VhdlParser::Type::Boolean.new
    end
    protected
    def entity
      nil
    end

    def basename
      ''
    end
  end
end
