module Copper::Circuit
  class RecordElementPort < Port
    def name
      case @name
      when String, Symbol
        @root.is_a?(Port) ? (@root.name + '.' + @name) : @name
      else
        @root.is_a?(Port) ? (@root.name + "(#@name)") : "(#@name)"
      end
    end

    protected
    def entity
      @root.entity
    end
  end
end
