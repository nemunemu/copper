module Copper::Circuit
  class Entity
    def self.from_entity_parser(vhdl_sources, entity_parser)
      ports = entity_parser.ports.map { |pt| lambda { |parent| Port.from_element(parent, pt) } }
      generics = entity_parser.generics.map { |gn| lambda { |parent| Generic.from_element(parent, gn) } }
      packages = entity_parser.packages
      components = entity_parser.components.map(&:name)
      new(vhdl_sources, entity_parser.name, ports, generics, packages, components)
    end

    attr_reader :root, :name, :ports, :generics
    def initialize(root, name, ports, generics, packages = [], components = [])
      @root = root
      @name = name
      @ports = ports.map { |pt| pt.is_a?(Proc) ? pt.call(self) : pt }
      @generics = generics.map { |gn| gn.is_a?(Proc) ? gn.call(self) : gn }
      @package_names = packages
      @component_names = components
    end

    def component_pathes(env)
      return @component_pathes if @component_pathes
      my_pathes = @component_names.map { |comp| env.path_of_entity(comp.to_s) }
      comp_pathes = components(env).compact.map { |comp| comp.component_pathes(env) }
      @component_pathes = [my_pathes, comp_pathes].flatten.uniq.compact
    end

    def package_pathes(env)
      Copper::Helper.log("searching packages of #{name}")
      return @package_pathes if @package_pathes
      my_pathes = @package_names.map { |pack| env.path_of_package(pack.to_s) }
      pack_pathes = packages(env).compact.map { |pack| pack.package_pathes(env) }
      comp_packs = components(env).compact.map { |comp| comp.package_pathes(env) }
      @package_pathes = [my_pathes, pack_pathes, comp_packs].flatten.uniq.compact
    end

    def components(env)
      return @components if @components
      Copper::Helper.log("loading components of #{name}: #{@component_names}")
      @components ||= @component_names.map { |comp| env.find_entity(comp.to_s) }
      Copper::Helper.log("loaded components of #{name}: #{@components.map(&:name)}")
      @components
    end

    def packages(env)
      @packages ||= @package_names.map { |pack| env.find_package(pack.to_s) }
    end
  end
end
