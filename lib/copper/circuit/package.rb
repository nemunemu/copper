module Copper::Circuit
  class Package
    attr_reader :root, :name, :constants
    def self.from_package_parser(vhdl_sources, package_parser)
      name = package_parser.name
      constants = package_parser.constants
      packages = package_parser.packages
      types = package_parser.types
      new(vhdl_sources, name, constants, packages, types)
    end

    attr_reader :root, :name, :constants, :types
    def initialize(root, name, constants, packages, types)
      @root = root
      @name = name
      @constants = constants
      @package_names = packages
      @types = types
    end

    def package_pathes(env)
      return @package_pathes if @package_pathes
      my_pathes = @package_names.map { |pack| env.path_of_package(pack.to_s) }
      pack_pathes = packages(env).map { |pack| pack.package_pathes(env) }
      @package_pathes = [my_pathes, pack_pathes].flatten.uniq.compact
    end

    def packages(env)
      @packages ||= @package_names.map { |pack| env.find_package(pack.to_s) }
    end
  end
end
