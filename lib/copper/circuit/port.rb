module Copper::Circuit
  class Port
    class << self
      include PortFactory
    end

    attr_reader :name, :type, :direction
    def initialize(root, name, direction, type)
      @root = root
      @name = name
      @direction = direction.to_sym
      @type = type
    end

    def root
      @root.root
    end

    def assign(value)
      formatter = lambda { |name, value| "#{name} <= #{value};" }
      format(formatter, value)
    end

    def equation(value)
      formatter = lambda { |name, value| "#{name} = #{value}" }
      format(formatter, value)
    end

    def actual(value)
      formatter = lambda { |name, value| "#{name} = 0x\" & to_hstring(#{name}) & \" (\" & to_string(#{name}) & \")" }
      format(formatter, value)
    end

    def format(formatter, value)
      formatter.call(name, type.format(value))
    end

    def fall
      assign(type.min)
    end

    def rise
      assign(type.max)
    end

    protected
    def entity
      @root
    end

    def basename
      @name
    end
  end
end
