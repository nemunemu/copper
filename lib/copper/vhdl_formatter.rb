require 'vhdl_connector'

module Copper
  module VhdlFormatter
    require 'copper/vhdl_formatter/port_ajuster'
    require 'copper/vhdl_formatter/generic_ajuster'
    require 'copper/vhdl_formatter/entity_wrapper'
    require 'copper/vhdl_formatter/signal_definition_presenter'
    require 'copper/vhdl_formatter/mapping_presenter'

    def self.component_definition(entity)
      EntityWrapper.new(entity).to_component_definition
    end

    def self.signal_definitions(entity)
      EntityWrapper.new(entity).to_component_signal_definition
    end

    def self.mappings(entity)
      EntityWrapper.new(entity).to_component_mapping
    end
  end
end
