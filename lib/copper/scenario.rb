module Copper
  module Scenario
    require 'copper/scenario/assert'
    require 'copper/scenario/assign'
    require 'copper/scenario/test_step'
    require 'copper/scenario/circuit'
    require 'copper/scenario/runner_script'
    require 'copper/scenario/description'
    require 'copper/scenario/test_bench_presenter'
    require 'copper/scenario/scenario'
    require 'copper/scenario/wait'

    class << self
      include Copper::Scenario
    end

    def generate(description)
      presenters = TestBenchPresenter.from_description(description)
      Scenario.new(description, presenters)
    end

    def circuit(path, &block)
      Circuit.new(path, &block)
    end

    def read(pathes)
      pathes.each { |path| load path }
    end
  end
end
