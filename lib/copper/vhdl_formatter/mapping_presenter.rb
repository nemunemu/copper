module Copper::VhdlFormatter
  class MappingPresenter < VhdlConnector::Presenters::ComponentMappingPresenter
    private
    def signal_name(port)
      (as = @aliases[port.name.to_sym]) ? as.to_s : port.name
    end
  end
end
