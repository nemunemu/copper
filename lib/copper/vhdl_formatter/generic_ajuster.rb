module Copper::VhdlFormatter
  class GenericAjuster
    extend Forwardable
    def_instance_delegators :@generic,
      :name, :direction
    def_instance_delegators '@generic.type',
      :left, :right

    def initialize(generic)
      @generic = generic
    end

    def type
      @generic.type.name
    end

    def size_dir
      @generic.type.direction
    end

    def value
      nil
    end
  end
end
