module Copper::VhdlFormatter
  class EntityWrapper < VhdlConnector::EntityWrapper
    def to_component_signal_definition(options = {})
      VhdlConnector::View.new(
        template_path('signal_definition.vhd.erb'),
        SignalDefinitionPresenter.new(self)
      ).run
    end

    def to_component_mapping(options = {})
      options.merge!(disable: :generic)
      VhdlConnector::View.new(
        template_path('component_mapping.vhd.erb'),
        MappingPresenter.new(self, options)
      ).run
    end

    def ports
      @ports ||= @entity.ports.map { |g| VhdlConnector::PortWrapper.new(PortAjuster.new(g)) }
    end

    def generics
      @generics ||= @entity.generics.map { |g| VhdlConnector::GenericWrapper.new(GenericAjuster.new(g)) }
    end
  end
end
