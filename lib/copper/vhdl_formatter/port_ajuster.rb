module Copper::VhdlFormatter
  class PortAjuster
    extend Forwardable
    def_instance_delegators :@port,
      :name, :direction
    def_instance_delegators '@port.type',
      :left, :right

    def initialize(port)
      @port = port
    end

    def type
      @port.type.name
    end

    def size_dir
      @port.type.direction
    end
  end
end
