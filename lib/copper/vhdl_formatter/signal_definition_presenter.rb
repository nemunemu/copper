module Copper::VhdlFormatter
  class SignalDefinitionPresenter < VhdlConnector::Presenters::SignalDefinitionPresenter
    def port_definition(port)
      "signal #{port.name} : #{port.type_description};"
    end
  end
end
