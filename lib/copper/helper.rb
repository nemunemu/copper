module Copper
  module Helper
    class << self
      include Helper
    end

    def from_root_path(path = '.')
      File.expand_path(path, File.expand_path('../../../', __FILE__))
    end

    def log(*str)
      STDERR.puts(*str) if Copper.debug_mode?
    end
  end
end
