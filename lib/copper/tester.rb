module Copper
  module Tester
    require 'copper/tester/formatters'
    require 'copper/tester/result_parser'
    require 'copper/tester/test_runner'

    class << self
      include Tester
    end

    def run(scenarios, config = {})
      config = default_options(config || {})
      formatter = Formatters::BaseFormatter.new(config[:output])
      formatter.dump_run_option(config)
      results = []
      scenarios.each do |scenario|
        formatter.dump_test_header(scenario.dut_name, scenario.dir)
        scenario.do_before
        test_log = TestRunner.new(scenario.runner_script, scenario.dir).run
        # formatter.dump_log(test_log)
        results << ResultParser.new(test_log, scenario.dut_name, scenario.dir)
      end
      results.each { |result| formatter.dump_failures(result) }
    end

    def default_options(custom_config = {})
      OpenStruct.new({
        include_tags: [],
        exclude_tags: [],
        output: STDOUT,
      }.merge(custom_config.to_h))
    end
  end
end
