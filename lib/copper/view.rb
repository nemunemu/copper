module Copper
  class View
    attr_reader :path
    def initialize(template_path, presenter)
      @template_path = File.expand_path(template_path, Helper.from_root_path('templates'))
      @presenter = presenter
    end

    def run
      code = template
      @result = @presenter.instance_eval { code.result(binding) }
    end

    def template
      ERB.new(File.read(@template_path), nil, '-')
    end

    def save(path)
      @path = path
      File.write(path, run)
      self
    end
  end
end
