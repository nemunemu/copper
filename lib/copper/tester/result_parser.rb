module Copper::Tester
  class TestFailure
    def description
      "#{format_header}\n#{format_body}"
    end

    protected
    def format_header
      "#@dutname"
    end
  end

  class AssertFailure < TestFailure
    def initialize(dutname, tmpdir, context, time, cond, expected, actual)
      @dutname, @tmpdir, @context, @time, @cond, @expected, @actual =
        dutname, tmpdir, context, time, cond, expected, actual
    end

    def format_header
      "#@dutname #@context (Test directory: #@tmpdir, Time: #@time)"
    end

    def compile_error?
      false
    end

    private
    def format_body
      body = []
      body << "FAILED: #@cond"
      body << "  expected: #@expected"
      body << "    actual: #@actual"
      body.join("\n")
    end
  end

  class CompileFailure < TestFailure
    def initialize(dutname, tmpdir, test_log = "")
      @dutname, @tmpdir, @test_log =
        dutname, tmpdir, test_log
    end

    def compile_error?
      true
    end

    def description
      "#{format_header}\n#{format_body}\n\n#@test_log"
    end

    def format_body
      "FAILED: Test did not run because of compilation error"
    end
  end

  class ExceptionFailure < TestFailure
    def initialize(dutname, exception)
      @dutname, @exception =
        dutname, exception
    end

    def compile_error?
      true
    end

    def description
      "#{format_header}\n#{format_body}\n\n#{backtrace}"
    end

    def backtrace
      "#{@exception.backtrace.first}: #{@exception.message}\n\tfrom #{@exception.backtrace[1..-1].join("\n\tfrom ")}"
    end

    def format_body
      "FAILED: Test did not run because of uncaught exception"
    end

    def format_header
      "#@dutname"
    end
  end

  class ResultParser
    def self.parse(runner, output)
      new(output, scenario.dut.name, scenario.tmpdir)
    end

    def self.fail(scenario, exception)
      new(nil, scenario.dut.name, nil).fail(exception)
    end

    def initialize(output, dutname, tmpdir)
      @dutname, @tmpdir, @output = dutname, tmpdir, output
    end

    def fail(exception)
      @failures = [ExceptionFailure.new(@dutname, exception)]
      self
    end

    def description
      failures.map(&:description).join("\n\n")
    end

    def failures
      return @failures ||= [CompileFailure.new(
        @dutname, @tmpdir, @output)] if compile_error?

      @failures ||= lines.reduce([]) do |failures, l|
          if l.match(/FAILED: (.*) expected to (.*), but (.*)/)
            cond, expected, actual = $1, $2, replace_binary($3)
            context = $1 if l.match(/In context (.*), FAILED:/)
            time = $1 if l.match(/:@(.*ps):\(assertion warning\)/)
            failures << AssertFailure.new(
              @dutname, @tmpdir, context, time, cond, expected, actual)
          end
          failures
      end
    end

    def compile_error?
      @output.nil? || @output.include?("compilation error") || @output.include?("Please elaborate your design")
    end

    def succeeded?
      ! compile_error? && ! failed?
    end

    def failed?
      @output.include?("FAILED") || compile_error?
    end

    # convert binary expression in a string to decimal
    def replace_binary(str)
      str.gsub(/[01]+/) { |bin| bin.to_i(2).to_s(10) }
    end

    private
    def lines
      @output.split("\n")
    end
  end
end
