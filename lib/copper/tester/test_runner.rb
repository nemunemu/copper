module Copper::Tester
  class TestRunner
    attr_reader :test_dir
    def initialize(runner_script, dir)
      @test_dir = dir
      @path = File.join(dir, "run.sh")
      runner_script.save(@path)
    end

    def run
      @output ||= IO.popen("sh #{@path} 2>&1") do |f|
        output = f.read
      end
    end
  end
end
