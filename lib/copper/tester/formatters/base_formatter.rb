module Copper::Tester::Formatters
  class BaseFormatter
    def initialize(output)
      @out = output
    end

    def dump_test_header(dutname, test_dir)
      @out << "#{dutname} (Test directory: #{test_dir})"
      @out << "\n"
    end

    def dump_log(log)
      @out << log
      @out << "\n\n"
    end

    def dump_run_option(config)
      return if config.include_tags.empty? && config.exclude_tags.empty?
      @out << "Run options:"
      @out << " include {#{config.include_tags.map(&:inspect).join(", ")}}" unless config.include_tags.empty?
      @out << " exclude {#{config.exclude_tags.map(&:inspect).join(", ")}}" unless config.exclude_tags.empty?
      @out << "\n\n"
    end

    def dump_failures(result)
      failures = result.failures
      @out << "\nFailures: \n\n" unless failures.empty?
      failures.each.with_index(1) do |failure, index|
        @out << "#{index}) "
        @out << failure.description
        @out << "\n\n"
      end
      @out << "#{failures.length} failures\n\n"
    end
  end
end
