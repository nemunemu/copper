module Copper::VhdlParser
  class NameParser
    NAME_REGEXP =
      /(?<name>[a-zA-Z_0-9]+)\s*,?\s*/im
    NAME_BLOCK_REGEXP =
      /(?<name_block>(?:#{NAME_REGEXP})+)/im

    class << self
      def parse_names(str)
        NAME_REGEXP.match_all(str).map { |m| m[:name] }
      end
    end
  end
end

