module Copper::VhdlParser::Type
  class Subtype
    attr_reader :name
    def initialize(name)
      @name = name
    end

    def primitive?
      false
    end

    def origin(env)
      env.find_origin_type!(name).origin(env)
    end
  end
end
