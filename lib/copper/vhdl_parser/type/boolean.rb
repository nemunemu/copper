module Copper::VhdlParser::Type
  class Boolean < Primitive
    def max
      true
    end

    def min
      false
    end
  end
end
