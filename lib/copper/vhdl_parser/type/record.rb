module Copper::VhdlParser::Type
  class Record < Primitive
    class << self
      def from_hash(name, key_types)
        new(name, key_types.map { |name, type| Copper::VhdlParser::Element::RecordElement.new(name, type) })
      end
    end

    attr_reader :name, :elements
    def initialize(name, elements)
      @name = name
      @elements = elements
    end

    def origin(env)
      els = @elements.map do |el|
        Copper::VhdlParser::Element::RecordElement.new(el.name, el.type.origin(env))
      end
      self.class.new(name, els)
    end
  end
end
