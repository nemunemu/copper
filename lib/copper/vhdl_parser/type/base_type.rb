module Copper::VhdlParser::Type
  class BaseType
    def format(value)
      value.to_s
    end

    def direction; nil; end
    def left; nil; end
    def right; nil; end
  end
end
