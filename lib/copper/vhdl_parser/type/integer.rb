module Copper::VhdlParser::Type
  class Integer < Primitive
    def format(value)
      value.to_i.to_s
    end
  end
end
