module Copper::VhdlParser::Type
  class Array < Primitive
    attr_reader :name, :length, :type
    def initialize(name, length, direction, type)
      @name = name
      @length = length.to_i
      @direction = direction.to_sym
      @type = type
    end

    def origin(env)
      self.class.new(@name, @length, @direction, @type.origin(env))
    end

    def elements
      @elements ||= length.times.map { |i| Copper::VhdlParser::Element::RecordElement.new(i, type) }
    end
  end
end
