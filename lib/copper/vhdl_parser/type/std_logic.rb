module Copper::VhdlParser::Type
  class StdLogic < Primitive
    def format(value)
      case value
      when String
        value
      else
        if [0, 1].include? value.to_i
          %Q{'#{value.to_i}'}
        else
          raise ValueTypeException, "unacceptable value error #{value}"
        end
      end
    end
  end
end
