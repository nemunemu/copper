module Copper::VhdlParser::Type
  class Primitive < BaseType
    def format(value)
      value ? 'true' : 'false'
    end

    def name
      self.class.name.split('::').last.camelize
    end

    def primitive?
      true
    end

    def origin(env)
      self
    end
  end
end
