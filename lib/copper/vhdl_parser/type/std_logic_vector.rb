module Copper::VhdlParser::Type
  class StdLogicVector < Primitive
    class << self
      def from_array_definition(left, direction, right)
        new((left.to_i - right.to_i).abs + 1, direction)
      end
    end

    attr_reader :length, :direction
    def initialize(length, direction = :downto)
      @length = length.to_i
      @direction = direction.to_sym
    end

    def left
      direction == :to ? 0 : length - 1
    end

    def right
      direction == :to ? length - 1 : 0
    end

    def format(value)
      case value
      when String
        value
      when Float
        v_to_int =  [value].pack("f").unpack("I").first
        format(v_to_int)
      else
        '"' + (2**@length + value).to_s(2)[-@length.. -1]+ '"'
      end
    end
  end
end
