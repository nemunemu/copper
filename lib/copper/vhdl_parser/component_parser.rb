module Copper::VhdlParser
  class ComponentParser < EntityDefinitionParser
    require 'copper/vhdl_parser/typedef_parser'
    COMPONENT_REGEXP =
      /component\s*(?<component_name>[a-zA-Z_0-9]*)\s*(?>(?>#{GENERIC_REGEXP})?)\s*(?>#{PORT_REGEXP})\s*end\s+component\s*;/im

    COMPONENT_NAME_REGEXP =
      /component\s*(?<component_name>[a-zA-Z_0-9]+)\s*/im

    class << self
      def scan(code)
        codes = code.split(/end\s+component\s*;/)[0..-2].map { |str| str + 'end component;' }
        codes.map do |code|
          COMPONENT_NAME_REGEXP.match_all(code).map do |match_data|
            Copper::Helper.log("scanning component #{match_data[:component_name]}")
            new(match_data.to_s)
          end
        end.flatten.compact
      end
    end

    def initialize(code)
      @code = code
    end

    def name
      @name ||= COMPONENT_NAME_REGEXP.match(@code)[:component_name].downcase
    end
  end
end
