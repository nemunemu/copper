module Copper::VhdlParser
  module Type
    class ValueTypeException < Exception; end
    require 'copper/vhdl_parser/type/base_type'
    require 'copper/vhdl_parser/type/primitive'

    require 'copper/vhdl_parser/type/integer'
    require 'copper/vhdl_parser/type/boolean'
    require 'copper/vhdl_parser/type/std_logic'
    require 'copper/vhdl_parser/type/std_logic_vector'

    require 'copper/vhdl_parser/type/subtype'
    require 'copper/vhdl_parser/type/record'
    require 'copper/vhdl_parser/type/array'
  end
end
