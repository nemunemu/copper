module Copper::VhdlParser
  module Element
    require 'copper/vhdl_parser/element/constant'
    require 'copper/vhdl_parser/element/port'
    require 'copper/vhdl_parser/element/generic'
    require 'copper/vhdl_parser/element/record_element'
  end
end
