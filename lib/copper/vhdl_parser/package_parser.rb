module Copper::VhdlParser
  class PackageParser
    include ParseLibrary

    CONSTANT_FOOTER =
      /(?<constant_type>[a-zA-Z_0-9]*)\s*(?::=\s*(?<constant_init>.*?))?\s*;/im
    CONSTANT_REGEXP =
      /constant\s*(?<constant_name>[a-zA-Z_0-9]*)\s*:\s*#{CONSTANT_FOOTER}/im

    PACKAGE_REGEXP =
      /package\s*(?<package_name>[a-zA-Z_0-9]*)\s*is\s+(?<package_decs>.*?)end\s*\k<package_name>\s*;/im

    class << self
      def scan(code)
        PACKAGE_REGEXP.match_all(code).map do |match_data|
          Copper::Helper.log("scanning package #{match_data[:package_name]}")
          new(match_data.to_s, code)
        end
      end
    end

    def initialize(code, fullcode = code)
      @code = code
      @fullcode = fullcode
      @typedef_parser = TypedefParser.new(code)
    end

    def name
      @name ||= PACKAGE_REGEXP.match(@code)[:package_name]
    end

    def constants(code = @code)
      @constants ||= CONSTANT_REGEXP.match_all(code).map do |match_data|
        Element::Constant.new(
          match_data[:constant_name],
          TypedefParser.parse_type(match_data[:constant_type]),
          match_data[:constant_init]
        )
      end
    end

    def packages
      @packages ||= parse_packages(@fullcode)
    end

    def types
      @types ||= TypedefParser.new(@code).types
    end
  end
end
