module Copper::VhdlParser
  class FileParser
    class << self
      def read(path)
        new(File.read(path), path)
      end

      def remove_comments(code)
        (code ? code : "").gsub(/--.*$/, '')
      end
    end

    attr_reader :path
    def initialize(code, path = nil)
      @path = path
      @code = self.class.remove_comments(code).downcase
    end

    def entities
      @entities ||= EntityParser.scan(@code)
    end

    def packages
      @packages ||= PackageParser.scan(@code)
    end
  end
end
