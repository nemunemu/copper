module Copper::VhdlParser
  module ParseLibrary
    LIBRARY_REGEXP =
      /use\s+(?<library_name>[a-zA-Z_0-9]*)\.(?<package_name>[a-zA-Z_0-9]*)\.(?<use_range>[a-zA-Z_0-9]*);/im

    def parse_packages(code)
      LIBRARY_REGEXP.match_all(code).map do |match_data|
        next unless match_data[:library_name] =~ /^work$/im
        match_data[:package_name]
      end.compact.flatten
    end
  end
end


