module Copper::VhdlParser
  class EntityParser < EntityDefinitionParser
    include ParseLibrary

    ENTITY_REGEXP =
      /entity\s*(?<entity_name>[a-zA-Z_0-9]*)\s*is\s+(?>#{GENERIC_BLOCK_REGEXP})?\s*(?>#{PORT_BLOCK_REGEXP})\s*end\s+\k<entity_name>\s*;/im
    LIBRARY_REGEXP =
      /use\s+(?<library_name>[a-zA-Z_0-9]*)\.(?<package_name>[a-zA-Z_0-9]*)\.(?<use_range>[a-zA-Z_0-9]*);/im

    class << self
      def scan(code)
        # TODO: parse entity architecture
        last_end = 0
        ENTITY_REGEXP.match_all(code).map do |match_data|
          Copper::Helper.log("scanning entity #{match_data[:entity_name]}")
          nextl = code.match(LIBRARY_REGEXP, match_data.end(0))
          nexts = nextl ? nextl.begin(0) : -1
          new(match_data.to_s, code[last_end..(last_end = nexts)])
        end
      end
    end

    def initialize(code, fullcode = '')
      @code = code
      @fullcode = fullcode
    end

    def name
      @name ||= ENTITY_REGEXP.match(@code)[:entity_name].downcase
    end

    def components
      @components ||= parse_components(@fullcode)
    end

    def packages
      @packages ||= parse_packages(@fullcode)
    end

    def parse_components(code = @fullcode)
      ComponentParser.scan(code)
    end
  end
end
