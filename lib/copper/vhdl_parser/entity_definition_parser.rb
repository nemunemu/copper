module Copper::VhdlParser
  class EntityDefinitionParser
    PORT_REGEXP =
      /#{NameParser::NAME_BLOCK_REGEXP}\s*:\s*(?<port_direction>inout|in|out)\s*(?<port_type>.*?)\s*$/im
    GENERIC_REGEXP =
      /#{NameParser::NAME_BLOCK_REGEXP}\s*:\s*(?<generic_type>.*?)\s*(?::=\s*(?<generic_init>.*?))?\s*$/im

    PALENTHESES_REGEXP = /(?>([^()]*(\([^()]*?\))?)*)/

    PORT_BLOCK_REGEXP = /port\s*\((?<ports>#{PALENTHESES_REGEXP})\);/im
    GENERIC_BLOCK_REGEXP = /generic\s*\((?<generics>#{PALENTHESES_REGEXP})\);/im

    def initialize(code)
      @code = code
    end

    def ports
      @ports ||= parse_ports(@code)
    end

    def generics
      @generics ||= parse_generics(@code)
    end

    def parse_ports(code = @code)
      port_block(code).map { |port| parse_port_line(port) }.compact.flatten
    end

    def parse_port_line(port)
      m = PORT_REGEXP.match(port)
      NameParser.parse_names(m[:name_block]).map do |name|
        Element::Port.new(
          name,
          m[:port_direction].to_sym,
          TypedefParser.parse_type(m[:port_type])
        )
      end if m
    end

    def parse_generics(code = @code)
      generic_block(code).map do |port|
        parse_generic_line(port)
      end.compact.flatten
    end

    def parse_generic_line(port)
      m = GENERIC_REGEXP.match(port)
      NameParser.parse_names(m[:name_block]).map do |name|
        Element::Generic.new(
          name,
          TypedefParser.parse_type(m[:generic_type]),
          m[:generic_init]
        )
      end if m
    end

    private
    def port_block(code = @code)
      m = PORT_BLOCK_REGEXP.match(code)
      m ? m[:ports].split(';') : []
    end

    def generic_block(code = @code)
      m = GENERIC_BLOCK_REGEXP.match(code)
      m ? m[:generics].split(';') : []
    end
  end
end
