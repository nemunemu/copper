module Copper::VhdlParser
  class TypedefParser
    SUBTYPE_REGEXP =
      /subtype\s*(?<subtype_name>[a-zA-Z_0-9]*)\s*is\s+(?<subtype_decs>.*?);/im
    TYPE_HEADER = /type\s*(?<type_name>[a-zA-Z_0-9]*)\s*is/im
    PROP_REGEXP = /(?<prop_name>[a-zA-Z_0-9]*)\s*:\s*(?<prop_type>.*?);/
    RECORD_REGEXP = /#{TYPE_HEADER}\s*record\s*(?<props>(?:#{PROP_REGEXP}\s*)*)\s*end\s*record;/im
    ARRAY_REGEXP = /#{TYPE_HEADER}\s*array\s*\((?<left>\d+)\s*(?<direction>to|downto)\s*(?<right>\d+)\)\s*of\s*(?<type>.*?)\s*;/im

    STD_LOGIC_REGEXP = /std_logic/im
    BOOLEAN_REGEXP = /boolean/im
    INTEGER_REGEXP = /integer/im
    STD_LOGICV_REGEXP = /std_logic_vector\((?<left>\d+)\s*(?<direction>to|downto)\s*(?<right>\d+)\)/im

    class << self
      def parse_type(code)
        case code
        when BOOLEAN_REGEXP
          Type::Boolean.new
        when INTEGER_REGEXP
          Type::Integer.new
        when STD_LOGICV_REGEXP
          match_data = $~
          Type::StdLogicVector.from_array_definition(
            match_data[:left].to_i,
            match_data[:direction],
            match_data[:right].to_i
          )
        when STD_LOGIC_REGEXP
          # must be after std_logic_vector
          Type::StdLogic.new
        else
          Type::Subtype.new(code.strip)
        end
      end
    end

    def initialize(vhdl)
      @vhdl = vhdl
    end

    def types
      return @types if @types
      @types = subtypes
      @types += record_types
      @types += array_types
      Copper::Helper.log("scanning types #{@types.to_h.keys}")
      @types = @types.to_h
    end

    def subtypes
      @subtypes ||= parse_subtypes
    end

    def parse_subtypes(code = @vhdl)
      SUBTYPE_REGEXP.match_all(code).map do |match_data|
        [match_data[:subtype_name], self.class.parse_type(match_data[:subtype_decs])]
      end
    end

    def array_types
      @array_types ||= parse_array_types
    end

    def parse_array_types(code = @vhdl)
      ARRAY_REGEXP.match_all(code).map do |match_data|
        name = match_data[:type_name]
        length = (match_data[:left].to_i - match_data[:right].to_i).abs + 1
        type = self.class.parse_type(match_data[:type])
        [match_data[:type_name], Type::Array.new(name, length, match_data[:direction], type)]
      end
    end

    def record_types
      @record_types ||= parse_record_types
    end

    def parse_record_types(code = @vhdl)
      RECORD_REGEXP.match_all(code).map do |match_data|
        record_data = PROP_REGEXP.match_all(match_data[:props]).map do |m|
          [m[:prop_name], self.class.parse_type(m[:prop_type])]
        end.to_h
        name = match_data[:type_name]
        [match_data[:type_name], Type::Record.from_hash(name, record_data)]
      end
    end
  end
end
