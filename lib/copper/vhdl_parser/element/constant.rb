module Copper::VhdlParser::Element
  class Constant
    attr_reader :name, :type, :init_value
    def initialize(name, type, init_value = nil)
      @name = name
      @type = type
      @init_value = init_value
    end
  end
end
