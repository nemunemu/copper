module Copper::VhdlParser::Element
  class Port
    attr_reader :name, :direction, :type
    def initialize(name, direction, type)
      @name = name
      @direction = direction
      @type = type
    end
  end
end
