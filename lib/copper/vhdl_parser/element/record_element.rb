module Copper::VhdlParser::Element
  class RecordElement
    attr_reader :name, :type
    def initialize(name, type, init_value = nil)
      @name = name
      @type = type
      @init_value = init_value
    end
  end
end
