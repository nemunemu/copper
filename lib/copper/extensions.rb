class Regexp
  def match_all(string, pos = 0, &block)
    matches = Array.new
    while match_data = self.match(string, pos)
      matches << match_data
      next_pos = match_data.end(0)
      if next_pos == pos
        break
      else
        pos = next_pos
      end
    end
    block ? matches.each(&block) : matches
  end
end

class String
  def camelize
    tail = self[1..-1].gsub(/[A-Z]/) { |str| "_#{str.downcase}" }
    "#{self[0].downcase}#{tail}"
  end
end
