require 'ostruct'

module Copper
  class Options < OpenStruct
  end

  class CommandLineOptions < Options
    class << self
      def parse_command_line(args)
        require 'optparse'

        options = default_options
        opt = OptionParser.new do |opts|
          opts.banner = "Copper: VHDL automatic test tool"
          opts.define_head "Usage: copper <testfiles> [options]"
          opts.separator ""
          opts.separator "Options:"

          opts.on("-t", "--tag TAG", "Run scenarios with the specified tag, or exclude scenarios by adding ~ before the tag") do |v|
            if v.match(/^~/)
              exclude_tags << v.gsub(/^~/, "").gsub(/^:/, "").to_sym
            else
              include_tags << v.gsub(/^:/, "").to_sym
            end
          end

          opts.on("-V", "--verbose", "Show detail messages in running tests") do
            options[:verbose] = true
          end

          opts.on_tail("-?", "--help", "Show this message") do
            puts opts
            return
          end

          opts.on_tail("-v", "--version", "Show version") do
            puts Copper::VERSION
            return
          end
        end
        opt.parse!(args)

        options[:files] = args
        new(options)
      end

      def default_options
        {
          out: STDOUT,
          verbose: false,
        }
      end
    end
  end
end

