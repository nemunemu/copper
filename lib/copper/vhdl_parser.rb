module Copper
  module VhdlParser
    require 'copper/vhdl_parser/type'
    require 'copper/vhdl_parser/element'
    require 'copper/vhdl_parser/name_parser'
    require 'copper/vhdl_parser/file_parser'
    require 'copper/vhdl_parser/entity_definition_parser'
    require 'copper/vhdl_parser/parse_library'

    require 'copper/vhdl_parser/component_parser'
    require 'copper/vhdl_parser/typedef_parser'
    require 'copper/vhdl_parser/package_parser'
    require 'copper/vhdl_parser/entity_parser'

    class << self
      def read(path)
        FileParser.read(path)
      end
    end
  end
end
