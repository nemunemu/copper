module Copper
  class VhdlSources
    class << self
      def sources
        @sources ||= new
      end
    end

    def initialize
      @sources = []
    end

    def add(*pathes)
      @sources += pathes.map { |path| Copper::Helper.log("adding #{path}"); VhdlParser.read(path) }
      delete_cache
    end

    def delete_cache
      @entities = nil
      @packages = nil
    end

    def entities
      @entities ||= @sources.reverse.map(&:entities).flatten.compact.map do |e|
        Copper::Helper.log("loading entity #{e.name}")
        Circuit::Entity.from_entity_parser(self, e)
      end
    end

    def packages
      @packages ||= @sources.reverse.map(&:packages).flatten.compact.map do |e|
        Copper::Helper.log("loading package #{e.name}")
        Circuit::Package.from_package_parser(self, e)
      end
    end

    def constants
      packages.map(&:constants).flatten
    end

    def types
      packages.map(&:types).reduce { |result, item| result.merge(item) }
    end

    def find_origin_type(name)
      types.to_h[name]
    end

    def find_origin_type!(name)
      type = types.to_h[name]
      raise "unknown subtype #{name}" unless type
      type
    end

    def find_entity(name)
      entities.find { |en| en.name.downcase == name.to_s.downcase }
    end

    def find_package(name)
      packages.find { |pk| pk.name.downcase == name.to_s.downcase }
    end

    def path_of_entity(name)
      entity = @sources.find { |so| so.entities.any? { |en| en.name.downcase == name.downcase } }
      unless entity
        Copper::Helper.log "component #{name} is not found"
        nil
      else
        entity.path
      end
    end

    def path_of_package(name)
      @sources.find { |so| so.packages.any? { |en| en.name.downcase == name.downcase } }.path
    end
  end
end
