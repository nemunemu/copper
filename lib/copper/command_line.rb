module Copper
  class CommandLine
    attr_reader :options
    def initialize(args)
      @options = CommandLineOptions.parse_command_line(args)
      exit unless @options
    end

    def run
      Copper::Runner.runner.run(@options[:files], @options)
    end
  end
end
