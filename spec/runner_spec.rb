require 'spec_helper'

describe Copper::Runner do
  subject { described_class.runner.run(script_pathes, options) }
  let(:script_pathes) { [fixture_path('test_script.rb')] }
  let(:options) { Copper::Options.new.default_options } 

  it 'don\'t failed' do
    expect(subject).to_not raise_error(Exception)
  end


end
