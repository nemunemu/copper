require 'spec_helper'

Copper.load_vhdl(fixture_path('alu.vhd'))

Copper::Scenario::Circuit.configure(:alu).scenario do |dut|
  step {
    assign dut.a => 1, dut.b => 1, dut.control => 0
    assert dut.output => 1
  }
end

