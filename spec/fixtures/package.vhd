library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;


package package_fixture is
  constant jal_register : register_addr_type := "11111";
  subtype type_name is std_logic;

  type memory_pipe_record is record
    order : order_type;
    state : memory_state_type;
    index : std_logic_vector(1 downto 0);
  end record;

  type pipe_type is array(3 downto 0) of boolean;

  constant init_memory_record : memory_pipe_record := (
    order => (others => '0'),
    state => (others => '0')
  );
end package;
