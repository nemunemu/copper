$LOAD_PATH.unshift File.expand_path('../../lib', __FILE__)
require 'copper'

def read_fixture(path)
  File.read(fixture_path(path))
end

def fixture_path(path)
  File.expand_path(path, File.expand_path('../fixtures', __FILE__))
end
