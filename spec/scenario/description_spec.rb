require 'spec_helper'

describe Copper::Scenario::Description do
  let(:description) { described_class.new(dut) }
  describe 'step' do
    subject { description.step(&step_block).format(dut) }

    context 'when assign port <= 1' do
      let(:dut) { Copper::Scenario::Circuit.new(entity) }
      let(:entity) { Copper::Circuit::Entity.new(nil, 'entity', ports, []) }
      let(:ports) {
        [Copper::Circuit::Port.generator(
          'port', :in, Copper::VhdlParser::Type::Integer.new)] }
      let(:step_block) {
        tb = dut
        proc { assign(tb.ports.first => 1) }
      }

      it 'match port <= 1;' do
        expect(subject).to match(/port <= 1;/)
      end
    end

    context 'when assert port = 2' do
      let(:dut) { Copper::Scenario::Circuit.new(entity) }
      let(:entity) { Copper::Circuit::Entity.new(nil, 'entity', ports, []) }
      let(:ports) {
        [Copper::Circuit::Port.generator(
          'port', :in, Copper::VhdlParser::Type::Integer.new)] }
      let(:step_block) {
        tb = dut
        proc { assert(tb.ports.first => 2) }
      }

      it 'match port = 2' do
        expect(subject).to match(/port = 2/)
      end
    end
  end
end

