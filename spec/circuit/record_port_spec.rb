require 'spec_helper'

describe Copper::Circuit::RecordPort do
  subject(:port) { described_class.new(root, name, direction, Copper::VhdlParser::Type::Record.new('', record)) }
  describe '#assign' do
    subject { port.assign(value) }
    let(:root) { Copper::Circuit::Entity.new(nil, 'entity', [], []) }
    let(:direction) { :in }
    let(:name) { 'record' }

    # TODO move to each type's spec
    context 'record contains integer type' do
      let(:record) { [Copper::VhdlParser::Element::RecordElement.new('a', Copper::VhdlParser::Type::Integer.new)] }
      let(:value)  { {'a' => 123} }

      it { expect(subject).to eql(['record.a <= 123;']) }
    end
  end
end
