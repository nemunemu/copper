require 'spec_helper'

describe Copper::Circuit::Port do
  subject(:port) { described_class.new(entity, name, direction, type) }
  describe '#assign' do
    subject { port.assign(value) }
    let(:entity) { Copper::Circuit::Entity.new(nil, 'entity', [], []) }
    let(:direction) { :in }
    let(:name) { 'circuit' }

    # TODO move to each type's spec
    context 'type is integer' do
      let(:type) { Copper::VhdlParser::Type::Integer.new }
      let(:value)  { 123 }

      it { expect(subject).to eql('circuit <= 123;') }
    end

    context 'type is std_logic' do
      let(:type) { Copper::VhdlParser::Type::StdLogic.new }
      let(:value)  { 1 }

      it { expect(subject).to eql('circuit <= \'1\';') }
    end

    context 'type is boolean' do
      let(:type) { Copper::VhdlParser::Type::Boolean.new }
      let(:value)  { true }

      it { expect(subject).to eql('circuit <= true;') }
    end

    context 'type is std_logic_vector' do
      let(:type) { Copper::VhdlParser::Type::StdLogicVector.new(3, :downto) }
      let(:value)  { 3 }

      it { expect(subject).to eql('circuit <= "011";') }
    end
  end
end
