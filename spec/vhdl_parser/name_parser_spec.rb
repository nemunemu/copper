require 'spec_helper'

describe Copper::VhdlParser::NameParser do
  describe '.parse_names' do
    subject { described_class.parse_names(code) }

    context 'parse "a"' do
      let(:code) { 'a' }
      it { expect(subject).to eql(['a']) }
    end

    context 'parse "a, b"' do
      let(:code) { 'a, b' }
      it { expect(subject).to eql(['a', 'b']) }
    end
  end
end
