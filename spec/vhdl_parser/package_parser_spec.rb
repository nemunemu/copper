require 'spec_helper'

describe Copper::VhdlParser::PackageParser do
  let(:parser) { described_class.new(code) }
  let(:code) { read_fixture('package.vhd') }

  describe '#package' do
    context 'parse package_fixtures' do
      subject { parser.constants }

      it('return 2 constants') { expect(subject.length).to eql(2) }
      its(:name) do
        expect(subject[0].name).to eql('jal_register')
        expect(subject[1].name).to eql('init_memory_record')
      end
      its(:type) do
        expect(subject[0].type.name).to eql('register_addr_type')
        expect(subject[1].type.name).to eql('memory_pipe_record')
      end
    end
  end
end
