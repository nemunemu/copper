require 'spec_helper'

describe Copper::VhdlParser::TypedefParser do
  let(:parser) { described_class.new(code) }
  let(:code) { read_fixture('package.vhd') }

  describe '#subtypes' do
    context 'parse package_fixtures' do
      subject { parser.subtypes }
      it 'return 1 subtype' do
        expect(subject.length).to eql(1)
      end
      its(:name) { expect(subject.first.first).to eql('type_name') }
      its(:type) { expect(subject.first.last.name).to eql('std_logic') }
    end
  end

  describe '#array_types' do
    context 'parse package_fixtures' do
      subject { parser.array_types }
      it 'return 1 array' do
        expect(subject.length).to eql(1)
      end
      its(:name) { expect(subject.first.first).to eql('pipe_type') }

      context 'array' do
        subject(:array) { parser.array_types.first.last }
        its(:length) { expect(subject.length).to eql(4) }
        its(:type) do
          expect(array.type.name).to eql('boolean')
        end
      end
    end
  end
end
