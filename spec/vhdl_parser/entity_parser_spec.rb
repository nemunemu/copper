require 'spec_helper'

describe Copper::VhdlParser::EntityParser do
  let(:parser) { described_class.new(code) }
  let(:code) { read_fixture('entity.vhd') }

  describe '#parse_port_line' do
    subject { parser.parse_port_line(line) }
    context 'contain ","' do
      let(:line) { "a, b : in std_logic_vector(31 downto 0);" }

      it('return 2 port') { expect(subject.length).to eql(2) }
      its(:name) { expect(subject[0].name).to eql('a') }
      its(:name) { expect(subject[1].name).to eql('b') }
      its(:type) { expect(subject[0].type.name).to eql('std_logic_vector') }
      its(:type) { expect(subject[1].type.name).to eql('std_logic_vector') }
    end

    context 'not contain ","' do
      let(:line) { "a : in std_logic_vector(31 downto 0);" }

      it('return 1 port') { expect(subject.length).to eql(1) }
      its(:name) { expect(subject[0].name).to eql('a') }
      its(:type) { expect(subject[0].type.name).to eql('std_logic_vector') }
    end
  end

  describe '#ports' do
    context 'parse entity_fixture' do
      subject { parser.ports }

      it('return 2 constants') { expect(subject.length).to eql(6) }
      its(:name) { expect(subject[0].name).to eql('a') }
      its(:name) { expect(subject[3].name).to eql('output') }
      its(:type) { expect(subject[0].type.name).to eql('std_logic_vector') }
      its(:type) { expect(subject[-1].type.name).to eql('std_logic') }
    end
  end

  describe '#generics' do
    context 'parse entity_fixture' do
      subject { parser.generics }

      it('return 2 constants') { expect(subject.length).to eql(1) }
      its(:name) { expect(subject[0].name).to eql('hoge') }
      its(:type) { expect(subject[0].type.name).to eql('std_logic') }
    end
  end
end
